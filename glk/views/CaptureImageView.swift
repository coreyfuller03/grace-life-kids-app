/*
    CaptureImageView.swift
    glk

    Created by Corey Fuller on 6/17/20.
    Copyright © 2020 Grace Life Baptist Church. All rights reserved.
*/
import SwiftUI

struct CaptureImageView {
    @Binding var isShown: Bool
    @Binding var image: UIImage?
    @Binding var cameraSelected: Bool
  
    func makeCoordinator() -> Coordinator {
        return Coordinator(isShown: $isShown, image: $image, cameraSelected: $cameraSelected)
    }
}

extension CaptureImageView: UIViewControllerRepresentable {
    func makeUIViewController(context: UIViewControllerRepresentableContext<CaptureImageView>) -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.delegate = context.coordinator

        if context.coordinator.isCameraSelectedForCoordinator {
            picker.sourceType = .camera
        }
        return picker
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController,
        context: UIViewControllerRepresentableContext<CaptureImageView>) {
        // TODO: update the view controller?
    }

}
