/*
    ContentView.swift
    glk

    Created by Corey Fuller on 6/17/20.
    Copyright © 2020 Grace Life Baptist Church. All rights reserved.
*/
import SwiftUI

struct ContentView: View {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @State private var navBarHidden = false
    
    var body: some View {
        NavigationView {
            VStack(spacing: 0) {
                Spacer()
                    .frame(height: appDelegate.screenHeight/16)
                Image("glk-tree")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .edgesIgnoringSafeArea(.top)
                    .scaledToFit()
                ZStack {
                    Color.white.edgesIgnoringSafeArea(.top)
                    VStack(spacing: 0) {
                        HStack {
                            NavigationLink(destination: ScavengerView(navBarHidden: self.$navBarHidden)) {
                                Image("search-solid")
                                    .resizable()
                                    .scaledToFit()
                                    .padding(10)
                            }.buttonStyle(PlainButtonStyle())
                            NavigationLink(destination: FiftyTwoView(navBarHidden: self.$navBarHidden)) {
                                Image("book-solid")
                                    .resizable()
                                    .scaledToFit()
                                    .padding(10)
                                }.buttonStyle(PlainButtonStyle())
                        }
                        HStack {
                            NavigationLink(destination: GCView(navBarHidden: self.$navBarHidden)) {
                                Image("puzzle-piece-solid")
                                    .resizable()
                                    .scaledToFit()
                                    .padding(10)
                            }.buttonStyle(PlainButtonStyle())
                            NavigationLink(destination: FiftyTwoView(navBarHidden: self.$navBarHidden)) {
                                Image("palette-solid")
                                    .resizable()
                                    .scaledToFit()
                                    .padding(10)
                            }.buttonStyle(PlainButtonStyle())
                        }
                    }
                }
            }
            .navigationBarTitle("")
            .navigationBarHidden(self.navBarHidden)
            .onAppear(perform: {
                self.navBarHidden = true
            })
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
