/*
    FiftyTwoView.swift
    glk

    Created by Corey Fuller on 6/17/20.
    Copyright © 2020 Grace Life Baptist Church. All rights reserved.
*/
import SwiftUI

struct FiftyTwoView: View {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @State private var catechismList = (UIApplication.shared.delegate as! AppDelegate).catechismList
    
    @Binding var navBarHidden : Bool
    
    var body: some View {
        ZStack {
            VStack {
                ZStack {
                    ForEach(0..<self.catechismList.count, id: \.self) { index in
                        CardView(card: self.catechismList[index])
                        {
                            withAnimation {
                                self.removeCard(at: index)
                            }
                        }
                        .stacked(at: index, in: self.catechismList.count)
                    }
                }
            }
        }
        .navigationBarTitle("52 in 52")
        .onAppear() {
            self.navBarHidden = false
        }

//        .onSwipeLeft {
//
//        }
//        .onSwipeRight {
//
//        }
    }
            
//            Color.black.edgesIgnoringSafeArea(.top)
//            VStack(spacing: appDelegate.screenWidth/8) {
//                Text("52 in 52")
//                    .font(.largeTitle).foregroundColor(.white)
//
//                VStack(spacing: appDelegate.screenWidth/8) {
//                    Text(FiftyTwoView.example.prompt)
//                        .foregroundColor(Color.white)
//                        .font(.largeTitle)
//                    Text(FiftyTwoView.example.answer)
//                    .foregroundColor(Color.white)
//                    .font(.title)
//                }.frame(minWidth: 0, maxWidth:
//                    .infinity).padding()
//                .frame(height: appDelegate.screenWidth)
//                .background(Color.gray).opacity(0.5)
//            }
//
//        }
    
    func removeCard(at index: Int) {
        catechismList.remove(at: index)
    }
    
}

extension View {
    func stacked(at position: Int, in total: Int) -> some View {
        let offset = CGFloat(total - position)
        return self.offset(CGSize(width: 0, height: 0))
    }
}
