/*
    GCView.swift
    glk

    Created by Corey Fuller on 6/17/20.
    Copyright © 2020 Grace Life Baptist Church. All rights reserved.
*/
import SwiftUI
import FirebaseUI
import FirebaseFunctions

struct GCView: View {
    @Binding var navBarHidden : Bool

    @State var firstName = ""
    @State var lastName = ""
    @State var gc: String = ""
    @State var gcImage: UIImage? = nil
    @State var showCaptureImageView: Bool = false
    @State var cameraSelected: Bool = false
    @State var showingSheet: Bool = false
    
    @State var gcErrorMessage = ""
    @State var gcSubmitted = false
    
    @State private var gcSubmissionMethod = 0
    
    var gcOptions = ["Upload a picture", "Write it out"]
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
    var body: some View {
        ZStack {
            VStack(spacing: 0) {
                Spacer()
                    .frame(height: appDelegate.screenHeight/16)
                Image("glk-lake").resizable()
                    .edgesIgnoringSafeArea(.top)
                    .scaledToFit()
                Text(self.gcErrorMessage)
                
                Form {
                    Section(header: Text("My Gospel Connection").font(.largeTitle)) {
                        TextField("Fitst name", text: $firstName)
                     TextField("Last Name", text: $lastName)
                        Picker(selection: $gcSubmissionMethod, label: Text("How would you like to submit?")) {
                            
                            ForEach(0..<gcOptions.count) { index in
                                Text(self.gcOptions[index]).tag(index)
                            }
                            
                        }.pickerStyle(SegmentedPickerStyle())
                        
                    }
                    
                    Section {
                        if gcSubmissionMethod == 1 {
                            TextField("I connected the Gospel to...", text: $gc).lineLimit(3)
                        } else {
                            Button(action: {
                                self.showingSheet = true
                                
                            }) {
                                Text("Upload a picture")
                            }.foregroundColor(.blue).font(.headline).actionSheet(isPresented: $showingSheet) {
                                ActionSheet(title: Text("Upload a picture"), buttons: [.default(Text("Camera")) {self.openImagePicker(cameraSelected: true)}, .default(Text("Photo Library")) {self.openImagePicker(cameraSelected: false)},
                                                    .cancel(Text("Nevermind"))])
                            }
                            if gcImage != nil {
                                Image(uiImage: gcImage! ).resizable()
                                .frame(width: 250, height: 250)
                                .clipShape(Circle())
                                .overlay(Circle().stroke(Color.white, lineWidth: 4))
                                .shadow(radius: 10)
                            }
                        }
                        
                    }
                    Section {
                        Button(action: {
                            self.uploadImage()
                        }) {
                            HStack {
                                Text("Done")
                                Image(systemName: "checkmark")
                            }.foregroundColor(.blue).font(.headline)
                        }
                    }
                }
            }
            
            if (showCaptureImageView) {
                CaptureImageView(isShown: $showCaptureImageView, image: $gcImage, cameraSelected: $cameraSelected)
            }
        }.background(Color.black)
        .navigationBarTitle("")
        .onAppear() {
            self.navBarHidden = false
        }
    }
    
    func openImagePicker(cameraSelected: Bool) {
        self.cameraSelected = cameraSelected
        self.showCaptureImageView.toggle()
    }
    
    func uploadImage() {
        // let functions = Functions.functions()
        
        if self.firstName.isEmpty || self.lastName.isEmpty {
            self.gcErrorMessage = "Make sure to tell us who made the connection!"
            return
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let storage = Storage.storage()
        let storageRef = storage.reference()
        
        let refName = "\(firstName)_\(lastName)_\(dateFormatter.string(from: Date()))"
        
        if gcSubmissionMethod == 1 {
            if self.gc.isEmpty {
                self.gcErrorMessage = "Make sure to tell us the Gospel connection!"
                return
            }
            self.gcImage = nil
            self.gcErrorMessage = ""

            let gcInfoJson = storageRef.child("gc-info_\(refName).json")

            let JSON = """
            {
                "gospel_connection": "\(gc)",
            }
            """
            let jsonData = JSON.data(using: .utf8)!
            _ = gcInfoJson.putData(jsonData, metadata: nil) { (metadata, error) in
                guard metadata != nil else {
                if error != nil {
                    self.gcErrorMessage = "An error occurred uploading data"
                    print("An error occurred uploading data: " + error!.localizedDescription)
                }
                return
              }
            }
        } else {
            if self.gcImage == nil {
                self.gcErrorMessage = "Make sure to upload the picture of the Gospel connection!"
                return
            }
            self.gc = ""
            self.gcErrorMessage = ""
            
            let uploadImage = storageRef.child("\(refName).jpg")
            
            _ = uploadImage.putData((gcImage?.pngData())!, metadata: nil) { (metadata, error) in
                guard metadata != nil else {
                if error != nil {
                    self.gcErrorMessage = "An error occurred uploading image"
                    print("An error occurred uploading image: " + error!.localizedDescription)
                }
                return
              }
            }
        }
    }
    
}
