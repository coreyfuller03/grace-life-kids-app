/*
    CardView.swift
    glk

    Created by Corey Fuller on 8/2/20.
    Copyright © 2020 Grace Life Baptist Church. All rights reserved.
*/
import SwiftUI

struct CardView: View {
    @State private var showingAnswer = false
    @State private var offset = CGSize.zero
    
    var card: RootedInTruth
    var removal: (() -> Void)? = nil
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 25, style: .continuous)
                .fill(Color.white)
                .shadow(radius: 10)
            
            VStack {
                Text(card.question)
                    .font(Font.custom("ROADMOVIE", size: appDelegate.screenHeight/25))
                    .foregroundColor(.black)
                if (showingAnswer) {
                    Text(card.answer)
                        .font(Font.custom("ROADMOVIE", size: appDelegate.screenHeight/30))
                        .foregroundColor(.gray)
                }
            }
            .padding(20)
            .multilineTextAlignment(.center)
        }
        .frame(width: appDelegate.screenWidth * 3/4, height: appDelegate.screenHeight * 2/3)
        .rotationEffect(.degrees(Double(offset.width / 5)))
        .offset(x: offset.width * 5, y: 0)
        .opacity(2 - Double(abs(offset.width / 50)))
        .gesture(DragGesture()
            .onChanged { gesture in
                self.offset = gesture.translation
            }
            .onEnded { _ in
                if abs(self.offset.width) > 100 {
                    self.removal?()
                } else {
                    self.offset = .zero
                }
            }
        )
        .onTapGesture {
            self.showingAnswer.toggle()
        }
    }
}
