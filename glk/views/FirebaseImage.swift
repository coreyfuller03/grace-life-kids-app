/*
    FirebaseImage.swift
    glk

    Created by Corey Fuller on 6/24/20.
    Copyright © 2020 Grace Life Baptist Church. All rights reserved.
*/
import Foundation
import FirebaseUI
import SwiftUI

struct FirebaseImage: View {
    
    var placeholderName: String
    var imageView: UIImageView

    init(placeholderName: String, realImageName: String) {
        self.placeholderName = placeholderName
        let placeholderImage = UIImage(named: placeholderName)
        
        let storage = Storage.storage()
        let pathReference = storage.reference(withPath: realImageName)
        self.imageView = UIImageView()
        self.imageView.sd_setImage(with: pathReference, placeholderImage: placeholderImage)
    }
    
    var body: some View {
        guard let img = self.imageView.image else { return Image(self.placeholderName).renderingMode(.original).resizable() .edgesIgnoringSafeArea(.top)
        .scaledToFit() }
        return Image(uiImage: img).renderingMode(.original).resizable() .edgesIgnoringSafeArea(.top)
        .scaledToFit()
    }

}
