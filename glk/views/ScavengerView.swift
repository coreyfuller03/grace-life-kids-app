/*
    ScavengerView.swift
    glk

    Created by Corey Fuller on 6/17/20.
    Copyright © 2020 Grace Life Baptist Church. All rights reserved.
*/
import SwiftUI

struct ScavengerView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @Binding var navBarHidden : Bool
    
    let screenWidth90Percent = UIScreen.main.bounds.width * 0.9
    let scavengerImage = (UIApplication.shared.delegate as! AppDelegate).scavengerImage
    
    @State var doneScavenging = false
    
    var body: some View {
        VStack {
            scavengerImage
            HStack {
                Button(action: {
                    if self.scavengerImage.imageView.image != nil {
                        UIImageWriteToSavedPhotosAlbum(self.scavengerImage.imageView.image!, nil, nil, nil)
                    }
                   
                }) {
                     Text("Save Image")
                        .foregroundColor(.blue)
                        .font(.headline)
                    .frame(width: screenWidth90Percent/2, alignment: .trailing)
                }
            }
        }
        .navigationBarTitle("")
        .onAppear() {
            self.navBarHidden = false
        }
    }

}
