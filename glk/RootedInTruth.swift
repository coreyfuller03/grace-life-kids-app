/*
    AppDelegate.swift
    glk

    Created by Corey Fuller on 8/2/20.
    Copyright © 2020 Grace Life Baptist Church. All rights reserved.
*/
import SwiftUI

struct RootedInTruth: Codable {
    var question: String
    var answer: String
    var address: String
}
