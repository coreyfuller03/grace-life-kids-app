/*
    AppDelegate.swift
    glk

    Created by Corey Fuller on 6/17/20.
    Copyright © 2020 Grace Life Baptist Church. All rights reserved.
*/
import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var scavengerImage: FirebaseImage
    var scavengerHuntImage: FirebaseImage
    var screenHeight: CGFloat = 0
    var screenWidth: CGFloat = 0
    var catechismList: [RootedInTruth] = []
    
    override init() {
        let rootedInTruthPath = Bundle.main.url(forResource: "52in52", withExtension: "json")!
        do {
            let jsonData = try Data(contentsOf: rootedInTruthPath)
            let decoder = JSONDecoder()
            do {
                catechismList = try decoder.decode([RootedInTruth].self, from: jsonData)
            } catch {
                print(error.localizedDescription)
            }
        } catch {
            print(error)
        }
        
        FirebaseApp.configure()
        
        self.scavengerImage = FirebaseImage(placeholderName: "scavenger", realImageName: "media/scavenger.jpg")
        self.scavengerHuntImage = FirebaseImage(placeholderName: "scavenger-hunt", realImageName: "media/scavenger-hunt.png")
        
        super.init()
    }
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

}
