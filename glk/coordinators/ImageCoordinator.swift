/*
    ImageCoordinator.swift
    glk

    Created by Corey Fuller on 6/17/20.
    Copyright © 2020 Grace Life Baptist Church. All rights reserved.
*/
import SwiftUI

class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @Binding var isCoordinatorShown: Bool
    @Binding var imageInCoordinator: UIImage?
    @Binding var isCameraSelectedForCoordinator: Bool
    
    init(isShown: Binding<Bool>, image: Binding<UIImage?>, cameraSelected: Binding<Bool>) {
        _isCoordinatorShown = isShown
        _imageInCoordinator = image
        _isCameraSelectedForCoordinator = cameraSelected
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let unwrapImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        imageInCoordinator = unwrapImage
        isCoordinatorShown = false
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        isCoordinatorShown = false
    }
}
