# Grace Life Kids App

This app is an iOS mobile app written in Swift using SwiftUI.

### This application is intended for use by the children of Grace Life Baptist Church.

* TODO: add summary here

### How do I get set up? ###

* TODO: add info here:
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### TODO items ###

* Create the 52 in 52 functionality like I did in react native
* Create sermon scavenger hunt page
* Create new part of app for coloring/drawing (maybe linked from home page like scavenger hunt)
* Add image with link to RightNow Media
* Add link to FB page and yourgracelife.com for info/events
* Update fonts to use Gotham (or some other GL font)